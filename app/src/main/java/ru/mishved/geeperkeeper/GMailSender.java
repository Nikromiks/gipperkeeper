package ru.mishved.geeperkeeper;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;

import com.sun.mail.smtp.SMTPTransport;
import com.sun.mail.util.BASE64EncoderStream;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.URLName;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

/**
 * Created on 3/28/14.
 *
 * @author by Timofeev Mikhail
 */



public class GMailSender {
    public static final int GET_TOKEN_SUCCESFUL = 0;
    public static final int GET_TOKEN_FAIL = 1;
    private Session session;
    private String token;
    private String userMail;
    private String TAG = "geeperkeeper";

    public String getToken() {
        return token;
    }

    public String getUserMail() {
        return userMail;
    }

    public void initToken(Activity ctx, Handler handler) {
        final Handler handler1 = handler;

        AccountManager am = AccountManager.get(ctx);

        Account[] accounts = am.getAccountsByType("com.google");

        if (accounts.length == 0) {
            android.os.Message msg = new android.os.Message();
            msg.what = GET_TOKEN_FAIL;
            handler1.sendMessage(msg);
            return;
        }
        Account me = accounts[0]; //You need to get a google account on the device, it changes if you have more than one

        userMail = me.name;

        am.getAuthToken(me, "oauth2:https://mail.google.com/", null, ctx, new AccountManagerCallback<Bundle>(){
            @Override
            public void run(AccountManagerFuture<Bundle> result){
                try{
                    Bundle bundle = result.getResult();
                    token = bundle.getString(AccountManager.KEY_AUTHTOKEN);

                    android.os.Message msg = new android.os.Message();
                    msg.what = GET_TOKEN_SUCCESFUL;
                    handler1.sendMessage(msg);
                } catch (Exception e){
                    Log.d(TAG, e.getMessage());
                    android.os.Message msg = new android.os.Message();
                    msg.what = GET_TOKEN_FAIL;
                    handler1.sendMessage(msg);
                }
            }
        }, null);
    }


    public SMTPTransport connectToSmtp(String host, int port, String userEmail, String oauthToken, boolean debug) throws Exception {

        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.starttls.required", "true");
        props.put("mail.smtp.sasl.enable", "false");

        session = Session.getInstance(props);
        session.setDebug(debug);

        final URLName unusedUrlName = null;
        SMTPTransport transport = new SMTPTransport(session, unusedUrlName);
        // If the password is non-null, SMTP tries to do AUTH LOGIN.
        final String emptyPassword = null;

        // enable if you use this code on an Activity (just for test) or use the AsyncTask
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        transport.connect(host, port, userEmail, emptyPassword);

        byte[] response = String.format("user=%s\1auth=Bearer %s\1\1", userEmail, oauthToken).getBytes();
        response = BASE64EncoderStream.encode(response);

        transport.issueCommand("AUTH XOAUTH2 " + new String(response), 235);

        return transport;
    }

    public synchronized void sendMail(String subject, String body, String user, String oauthToken, String recipients) throws Exception {

            SMTPTransport smtpTransport = connectToSmtp("smtp.gmail.com", 587, user, oauthToken, true);

            MimeMessage message = new MimeMessage(session);
            DataHandler handler = new DataHandler(new ByteArrayDataSource(body.getBytes(), "text/plain"));
            message.setSender(new InternetAddress(user));
            message.setSubject(subject);
            message.setDataHandler(handler);
            if (recipients.indexOf(',') > 0)
                message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(recipients));
            else
                message.setRecipient(Message.RecipientType.TO,
                        new InternetAddress(recipients));
            smtpTransport.sendMessage(message, message.getAllRecipients());
    }

}