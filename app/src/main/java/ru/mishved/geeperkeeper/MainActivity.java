package ru.mishved.geeperkeeper;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.RadioButton;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

    private final String email_sender = "drops@ukeeper.com";
    private Handler handler;
    private String TAG = "geeperkeeper";
    private String fieldTypeSender = "TYPE_SENDER";
    private SharedPreferences share;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Get intent, action and MIME type
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSendText(intent); // Handle text being sent
            }
        }

        share = getPreferences(MODE_PRIVATE);
        TypeSender typeSender = TypeSender.valueOf(share.getString(fieldTypeSender, TypeSender.GMAIL.name()));

        View.OnClickListener radioListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                RadioButton rb = (RadioButton) v;
                SharedPreferences.Editor ed = share.edit();

                switch (rb.getId()) {
                    case R.id.radioButtonGmail:
                        ed.putString(fieldTypeSender, TypeSender.GMAIL.name());
                        ed.commit();
                        break;
                    case R.id.radioButtonMail:
                        ed.putString(fieldTypeSender, TypeSender.MAIL.name());
                        ed.commit();
                        break;
                    default:
                        break;
                }
            }
        };

        RadioButton radioButtonGmail = (RadioButton) findViewById(R.id.radioButtonGmail);
        radioButtonGmail.setOnClickListener(radioListener);
        RadioButton radioButtonMail = (RadioButton) findViewById(R.id.radioButtonMail);
        radioButtonMail.setOnClickListener(radioListener);

        switch (typeSender) {
            case GMAIL:
                radioButtonGmail.setChecked(true);
                break;
            case MAIL:
                radioButtonMail.setChecked(true);
                break;
        }

    }

    void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null && URLUtil.isHttpUrl(sharedText)) {

            SharedPreferences share = getPreferences(MODE_PRIVATE);
            TypeSender typeSender = TypeSender.valueOf(share.getString(fieldTypeSender, TypeSender.GMAIL.name()));
            switch (typeSender) {
                case GMAIL:
                    handleSendGmail(sharedText);
                    break;
                case MAIL:
                    handleSendMail(sharedText);
                    break;
            }
            finish();
        }
    }

    void handleSendMail(String bodyMessage) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{email_sender});
        i.putExtra(Intent.EXTRA_SUBJECT, "ukeeper " + bodyMessage);
        i.putExtra(Intent.EXTRA_TEXT, bodyMessage);

        Toast.makeText(MainActivity.this, "uKeeper emeail sending url. " + bodyMessage, Toast.LENGTH_LONG).show();

        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this, "There are no email clients installed.", Toast.LENGTH_LONG).show();
        }
    }

    void handleSendGmail(String bodyMessage) {
        final GMailSender sender = new GMailSender();
        final String bodyMessage1 = bodyMessage;

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case GMailSender.GET_TOKEN_SUCCESFUL:
                        try {
                            sender.sendMail("uKeeper", bodyMessage1, sender.getUserMail(), sender.getToken(), email_sender);
                        } catch (Exception e) {
                            Log.e(TAG, e.getMessage());
                            Toast.makeText(MainActivity.this, "Error send email", Toast.LENGTH_LONG).show();
                        }
                        Toast.makeText(MainActivity.this, "uKeeper emeail sending url. " + bodyMessage1, Toast.LENGTH_LONG).show();
                        break;
                    case GMailSender.GET_TOKEN_FAIL:
                        Toast.makeText(MainActivity.this, "Error send email", Toast.LENGTH_LONG).show();
                        break;
                }
            }
        };
        sender.initToken(this, handler);
    }
}
